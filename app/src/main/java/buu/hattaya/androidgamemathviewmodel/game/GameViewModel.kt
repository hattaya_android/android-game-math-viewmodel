package buu.hattaya.androidgamemathviewmodel.game

import android.os.CountDownTimer
import android.text.format.DateUtils
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import kotlin.random.Random

class GameViewModel : ViewModel() {

    private var _number1 = MutableLiveData<Int>()
    val number1: LiveData<Int>
    get() = _number1

    private var _number2 = MutableLiveData<Int>()
    val number2: LiveData<Int>
        get() = _number2

    private var _sign = MutableLiveData<String>()
    val sign: LiveData<String>
        get() = _sign

    private var _answer = MutableLiveData<Int>()
    val answer: LiveData<Int>
        get() = _answer

    private var _btn1 = MutableLiveData<Int>()
    val btn1: LiveData<Int>
        get() = _btn1

    private var _btn2 = MutableLiveData<Int>()
    val btn2: LiveData<Int>
        get() = _btn2

    private var _btn3 = MutableLiveData<Int>()
    val btn3: LiveData<Int>
        get() = _btn3

    // The current scoreRight
    private var _scoreRight = MutableLiveData<Int>()
    val scoreRight: LiveData<Int>
        get() = _scoreRight

    // The current scoreWrong
    private var _scoreWrong = MutableLiveData<Int>()
    val scoreWrong: LiveData<Int>
        get() = _scoreWrong

    private var _amount = MutableLiveData<Int>()
    val amount: LiveData<Int>
        get() = _amount

    // Event which triggers the end of the game
    private val _eventGameFinish = MutableLiveData<Boolean>()
    val eventGameFinish: LiveData<Boolean>
        get() = _eventGameFinish

    // Countdown time
    private val timer: CountDownTimer
    private val _currentTime = MutableLiveData<Long>()
    val currentTime: LiveData<Long>
        get() = _currentTime

    val currentTimeString = Transformations.map(currentTime) { time ->
        DateUtils.formatElapsedTime(time)
    }

    var maker = 0
    var choose = 0

    init {
        Log.i("GameViewModel", "GameViewModel created!")
        _scoreRight.value = 0
        _scoreWrong.value = 0
        _sign.value = ""
        _amount.value = -1
        timer = object : CountDownTimer(COUNTDOWN_TIME, ONE_SECOND) {
            override fun onTick(millisUntilFinished: Long) {
                _currentTime.value = millisUntilFinished / ONE_SECOND
            }
            override fun onFinish() {
                _currentTime.value = DONE
                onGameFinish()
            }
        }
        timer.start()
    }

    fun start(){
        _amount.value = _amount.value?.plus(1)
        var numberRandom1 =  Random.nextInt(0,9)
        var numberRandom2 =  Random.nextInt(0,9)
        _number1.value = numberRandom1
        _number2.value = numberRandom2
            if (maker == 1) {
                _answer.value = numberRandom1 + numberRandom2
            }
            if (maker == 2) {
                _answer.value = numberRandom1 - numberRandom2
            }
            if (maker == 3) {
                _answer.value = numberRandom1 * numberRandom2
            }

        val choice = Random.nextInt(0, 3)
        if (choice == 0) {
            _btn1.value = _answer.value
            _btn2.value = _answer.value?.plus(1)
            _btn3.value = _answer.value?.minus(1)
        } else if (choice == 1) {
            _btn1.value = _answer.value?.plus(1)
            _btn2.value = _answer.value
            _btn3.value = _answer.value?.minus(1)
        } else {
            _btn1.value = _answer.value?.minus(1)
            _btn2.value = _answer.value?.plus(1)
            _btn3.value = _answer.value
        }
    }

    fun checkSign(sign: String) {
        if (sign == "1") {
            _sign.value  = "+"
            maker = 1
            start()
        }
        if (sign == "2") {
            _sign.value = "-"
            maker = 2
            start()
        }
        if (sign == "3") {
            _sign.value = "*"
            maker = 3
            start()
        }
        if (sign == "4") {
            loop()
        }
    }

    fun loop(){
        choose = 1
            _amount.value = _amount.value?.plus(1)
            var numberRandom1 = Random.nextInt(0, 9)
            var numberRandom2 = Random.nextInt(0, 9)
            val allSign = Random.nextInt(0, 3)
            if (allSign == 1) {
                _sign.value = "+"
                _answer.value = numberRandom1 + numberRandom2
            } else if (allSign == 2) {
                _sign.value = "-"
                _answer.value = numberRandom1 - numberRandom2
            } else {
                _sign.value = "*"
                _answer.value = numberRandom1 * numberRandom2

            }

            _number1.value = numberRandom1
            _number2.value = numberRandom2

            val choice = Random.nextInt(0, 3)
            if (choice == 0) {
                _btn1.value = _answer.value
                _btn2.value = _answer.value?.plus(1)
                _btn3.value = _answer.value?.minus(1)
            } else if (choice == 1) {
                _btn1.value = _answer.value?.plus(1)
                _btn2.value = _answer.value
                _btn3.value = _answer.value?.minus(1)
            } else {
                _btn1.value = _answer.value?.minus(1)
                _btn2.value = _answer.value?.plus(1)
                _btn3.value = _answer.value
            }
    }

    fun checkButton(btn: Int) {
        if (btn == answer.value){
            _scoreRight.value = _scoreRight.value?.plus(1)
            checkChoose ()
        } else {
            _scoreWrong.value = _scoreWrong.value?.plus(1)
            checkChoose ()
        }

    }

    fun checkChoose () {
        if (choose == 0) {
            start()
        } else {
            loop()
        }
    }

    fun onGameFinish() {
        _eventGameFinish.value = true
    }
    fun onGameFinishComplete() {
        _eventGameFinish.value = false
    }

    companion object{
        // Time when the game is over
        private const val DONE = 0L

        // Countdown time interval
        private const val ONE_SECOND = 1000L

        // Total time for the game
        private const val COUNTDOWN_TIME = 60000L
    }

    override fun onCleared() {
        super.onCleared()
        Log.i("GameViewModel", "GameViewModel destroyed!")
        timer.cancel()
    }
}
