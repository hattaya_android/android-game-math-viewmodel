package buu.hattaya.androidgamemathviewmodel.game

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import buu.hattaya.androidgamemathviewmodel.R
import buu.hattaya.androidgamemathviewmodel.databinding.FragmentCategoryBinding

class CategoryFragment : Fragment() {
    private lateinit var binding: FragmentCategoryBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_category, container, false)

        var sign: Int = 0
        var onClick: Int = 0

        binding.btnPlus.setOnClickListener{
            sign = 1
            onClick = 1
        }

        binding.btnSubtract.setOnClickListener {
            sign = 2
            onClick = 1
        }

        binding.btnMultiply.setOnClickListener{
            sign = 3
            onClick = 1
        }

        binding.btnAll.setOnClickListener{
            sign = 4
            onClick = 1
        }

        binding.btnStart.setOnClickListener{
            if(onClick == 0) {
                Toast.makeText(activity, "Please select a Category", Toast.LENGTH_SHORT).show()
            } else {
                val action = CategoryFragmentDirections.actionCategoryFragmentToGameFragment(sign)
                it.findNavController().navigate(action)
            }
        }

        setHasOptionsMenu(true)
        return binding.root
    }
}