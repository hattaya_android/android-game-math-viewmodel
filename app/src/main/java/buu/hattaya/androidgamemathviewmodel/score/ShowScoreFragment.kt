package buu.hattaya.androidgamemathviewmodel.score

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import buu.hattaya.androidgamemathviewmodel.R
import buu.hattaya.androidgamemathviewmodel.databinding.FragmentShowScoreBinding

class ShowScoreFragment : Fragment() {
    private lateinit var binding : FragmentShowScoreBinding
    private lateinit var viewModel: ScoreViewModel
    private lateinit var viewModelFactory: ScoreViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate<FragmentShowScoreBinding>(inflater,
            R.layout.fragment_show_score, container,false)

        viewModelFactory = ScoreViewModelFactory(
            ShowScoreFragmentArgs.fromBundle(requireArguments()).scoreRight,
            ShowScoreFragmentArgs.fromBundle(requireArguments()).scoreWrong,
            ShowScoreFragmentArgs.fromBundle(requireArguments()).amount)
        viewModel= ViewModelProvider(this, viewModelFactory).get(ScoreViewModel::class.java)

        viewModel.eventNewGame.observe(viewLifecycleOwner, Observer { newGame ->
            if (newGame) {
                findNavController().navigate(ShowScoreFragmentDirections.actionShowScoreFragmentToCategoryFragment())
                viewModel.onNewGameComplete()
            }
        })

        binding.scoreViewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }
}