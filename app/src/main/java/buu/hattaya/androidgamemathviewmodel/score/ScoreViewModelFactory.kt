package buu.hattaya.androidgamemathviewmodel.score

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ScoreViewModelFactory(private val finalScoreRight: Int, private val finalScoreWrong: Int, private val amount: Int) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ScoreViewModel::class.java)) {
            return ScoreViewModel(finalScoreRight,finalScoreWrong, amount) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}