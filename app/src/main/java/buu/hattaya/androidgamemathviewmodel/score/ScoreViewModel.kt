package buu.hattaya.androidgamemathviewmodel.score

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ScoreViewModel(finalScoreRight: Int, finalScoreWrong: Int, amount: Int) : ViewModel(){
    // The final score
    private val _scoreRight = MutableLiveData<Int>()
    val scoreRight: LiveData<Int>
        get() = _scoreRight

    private val _scoreWrong = MutableLiveData<Int>()
    val scoreWrong: LiveData<Int>
        get() = _scoreWrong

    private val _amount = MutableLiveData<Int>()
    val amount: LiveData<Int>
        get() = _amount

    private val _eventNewGame = MutableLiveData<Boolean>()
    val eventNewGame: LiveData<Boolean>
        get() = _eventNewGame

    init {
        Log.i("ScoreViewModel", "Final amount is $amount")
        _amount.value = amount
        Log.i("ScoreViewModel", "Final score right is $finalScoreRight")
        _scoreRight.value = finalScoreRight
        Log.i("ScoreViewModel", "Final score wrong is $finalScoreWrong")
        _scoreWrong.value = finalScoreWrong
    }

    fun onNewGame(){
        _eventNewGame.value = true
    }

    fun onNewGameComplete(){
        _eventNewGame.value = false
    }
}